<?php 

/**
 * Prepare and generate entry form for Alfresco settings.
 * 
 */
function cmis_alfresco_ticket_admin_settings() {
  $form['cmis_alfresco_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Alfresco CMIS Settings'), 
    '#description' => t('Settings for Alfreso Server including Server Endpoint, Username and password for connection.'), 
    '#collapsible' => FALSE, 
    '#collapsed' => FALSE,
  );

  $form['cmis_alfresco_settings']['cmis_alfresco_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Repository endpoint'), 
    '#description' => t('Specify the Alfresco Endpoint to integrate with.'), 
    '#default_value' => variable_get('cmis_alfresco_endpoint', 'http://localhost:8080/alfresco'),
  );

  $form['cmis_alfresco_settings']['cmis_alfresco_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Repository user'), 
    '#description' => t('Specify the user name to use to authenticate with the repository.'), 
    '#default_value' => variable_get('cmis_alfresco_user', 'admin'),
  );

  $form['cmis_alfresco_settings']['cmis_alfresco_password'] = array(
    '#type' => 'password',
    '#title' => t('Repository password'), 
    '#description' => t('Specify the password to use to authenticate with the repository.'), 
    '#default_value' => ''
  );

  return system_settings_form($form);
}

/**
 * Validate Setting inputs for Alfresco module 
 * 
 */
function cmis_alfresco_ticket_admin_settings_validate($form, & $form_state) {
  

  // endpoint is required
  $endpoint = $form_state['values']['cmis_alfresco_endpoint'];
  if ($endpoint == '') {
    form_set_error('cmis_alfresco_endpoint', t('You must specify the Alfresco endpoint.'));
  }

  // see if the endpoint is okay
  /*    
      $response = drupal_http_request($endpoint);
      if ($response->code != 200) {
          form_set_error('cmis_alfresco_endpoint', t('Error received from endpoint: ' . $response->code));
      }
  */
  // user and password are required
  if (empty($form_state['values']['cmis_alfresco_user'])) {
    form_set_error('cmis_alfresco_user', t('You must specify the repository user.'));
  }

  if (empty($form_state['values']['cmis_alfresco_password'])) {
    form_set_error('cmis_alfresco_password', t('You must specify the repository password.'));
  }
  else {
    $form_state['values']['cmis_alfresco_password'] = base64_encode($form_state['values']['cmis_alfresco_password']);
  }

  /* 
   * DG: This can't be done with the current API because the values have not been persisted yet
   * 
      // attempt to authenticate
      if (cmis_alfresco_utils_get_ticket() == '') {
          form_set_error('', t('Could not authenticate with endpoint.'));
      } else {
          drupal_set_message('Alfresco Repo Info:<br/>' . cmis_alfresco_info());
      }
  */
}
