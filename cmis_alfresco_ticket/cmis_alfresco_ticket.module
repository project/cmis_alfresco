<?php 

/**
 * Implementation of hook_menu()
 * 
 */
function cmis_alfresco_ticket_menu() {
  $items['admin/settings/cmis/alfresco'] = array(
    'title' => 'Alfresco Settings',
    'description' => 'Connection settings to Alfresco repository',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cmis_alfresco_ticket_admin_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer alfresco'),
    'file' => 'cmis_alfresco_ticket.pages.inc'
  );
  
  return $items;
}

/**
 * Safely creates a path 
 * 
 * @return string
 */
function cmis_alfresco_ticket_join_path() {
  $args = func_get_args();
  $paths = array();
  foreach ($args as $arg) {
    $paths = array_merge($paths, (array)$arg);
  }
  foreach ($paths as &$path) {
    $path = trim($path, '/');
  }
  return join('/', $paths);
}

/**
 * Utility function for generating HTTP Basic Authentication header.
 *
 */
function cmis_alfresco_ticket_get_auth_headers() {
  $user = variable_get('cmis_alfresco_user', '');
  $pass = base64_decode(variable_get('cmis_alfresco_password', ''));

  return array('Authorization' => 'Basic '. base64_encode($user .':'. $pass));
}

/**
 * Utility function for getting alfresco ticket.
 *
 * @param $op Option for refreshing ticket or not.
 */
function cmis_alfresco_ticket_get_ticket($op = 'norefresh') {
  $ticket = $_SESSION['cmis_alfresco_ticket'];
  if ($ticket == NULL || $op == 'refresh') {

    $user = variable_get('cmis_alfresco_user', '');
    $pass = base64_decode(variable_get('cmis_alfresco_password', ''));

    // Authenticate and store the ticket
    $url = cmis_alfresco_ticket_get_url('/api/login?u='. $user .'&pw='. $pass);
    
    $response = drupal_http_request($url);

    if ($response->code == 200 || $response->code == 201) {
      $ticket_xml = new SimpleXMLElement($response->data);
      $ticket = (string) $ticket_xml[0];
    } 
    else {
      throw new CMISException(t('Error received from endpoint: @code', array('@code' => $response->code)));
    }

    if ($ticket == '') {
      throw new CMISException(t('Unable to authenticate with endpoint'));
    } 

    $_SESSION['cmis_alfresco_ticket'] = $ticket;
  } 

  return $ticket;
}

/**
 * Return service url for HTTP basic authentication.
 *
 */
function cmis_alfresco_ticket_get_url($url) {
  $endpoint = variable_get('cmis_alfresco_endpoint', '');
  return cmis_alfresco_ticket_join_path($endpoint, 'service', $url);
}

/**
 * Return service url for ticket based authentication.
 *  
 * @param $url
 * @param $op Option for refreshing ticket or not.
 */
function cmis_alfresco_ticket_get_wc_url($url, $op = 'norefresh') {
  $endpoint = variable_get('cmis_alfresco_endpoint', '');
  $wcendpoint = cmis_alfresco_ticket_join_path($endpoint, '/s');
  $ticket = cmis_alfresco_ticket_get_ticket($op);

  if (FALSE === strstr($url, '?')) {
    if ($url[0] == '/') {
      return cmis_alfresco_ticket_join_path($wcendpoint, $url .'?alf_ticket='. $ticket);
    }
    else {
      return $url .'?alf_ticket='. $ticket;
    }
  }

  return $wcendpoint . str_replace('?', '?alf_ticket='. $ticket .'&', $url);
}

/**
 * Invoke Alfresco Webscript based Service.
 *  
 * @param $serviceurl
 * @param $headers
 * @param $method
 * @param $data
 * @param $retry
 * @return string xml response
 */
function cmis_alfresco_ticket_cmis_alfresco_service($serviceurl, $headers = array(), $method = 'GET', $data = NULL, $retry = 3) {
  $response = cmis_alfresco_ticket_http_request($serviceurl, NULL, $headers, $method, $data, $retry);

  if (in_array($response->code, array(200,201,204))) {
    $content = $response->data;
    
    if (false === strstr($content, 'Alfresco Web Client - Login')) {
      return $content;
    } 
    else {
      $response2 = cmis_alfresco_ticket_http_request($serviceurl, 'refresh', $headers, $method, $data, $retry);
      if ($response2->code == 200 || $response->code == 201) {
        return $response2->data;
      }
      else {
        throw new CMISException(t('Failed to invoke service '. $serviceurl .' Code:'. $response2->code));
      }
    }
  }
  elseif ($response->code == 302 || $response->code == 505 || $response->code == 401) {
    $response2 = cmis_alfresco_ticket_http_request($serviceurl, 'refresh', $headers, $method, $data, $retry);

    if ($response2->code == 200 || $response->code == 201) {
      return $response2->data;
    } 
    else {
      throw new CMISException(t('Failed to invoke service '. $serviceurl .' Code:'. $response2->code));
    }
  } 
  else {
    throw new CMISException(t('Failed to invoke service '. $serviceurl .' Code:'. $response->code . $response->data));
  }
}

/**
 * Invoke Webscript based Service using curl apis.
 * It is intended to be a utility function that handles authentication (basic, ticket),
 * http headers (if necessary) and custom post ( cmisquery+xml, atom+xml etc.).
 * 
 * @param $serviceurl Alfreco webscript service url without /service or /wcservice prefix.
 * @param $auth Option for service authentication.
 * @param $headers Additional http headers for making the http call. 'ticket' is for ticket based, 'basic' for http basic authentication, 'refresh' for ticket based but with ticket refresh.
 * @param $method
 * @param $data
 * @param $retry
 * @return response stdClass object
 */
function cmis_alfresco_ticket_http_request($serviceurl, $auth = 'ticket', $headers = array(), $method = 'GET', $data = NULL, $retry = 3) {
  if ($auth == 'basic') {
    $url = cmis_alfresco_ticket_get_url($serviceurl);
  }
  elseif ($auth == 'refresh') {
    $url = cmis_alfresco_ticket_get_wc_url($serviceurl, 'refresh');
  } 
  else {
    $url = cmis_alfresco_ticket_get_wc_url($serviceurl);
  }

  // Prepare curl session
  $session = curl_init($url);
  curl_setopt($session, CURLOPT_VERBOSE, 1);

  // Add additonal headers
  curl_setopt($session, CURLOPT_HTTPHEADER, $headers);

  // Don't return HTTP headers. Do return the contents of the call
  curl_setopt($session, CURLOPT_HEADER, FALSE);
  curl_setopt($session, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($session, CURLOPT_CONNECTTIMEOUT, 4);

  if ($auth == 'basic') {
    $user = variable_get('cmis_alfresco_user', '');
    $pass = variable_get('cmis_alfresco_password', '');
    curl_setopt($session, CURLOPT_USERPWD, "$user:$pass");
  }

  if ($method == 'CUSTOM-POST') {
    curl_setopt($session, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($session, CURLOPT_POSTFIELDS, $data);
    curl_setopt($session, CURLOPT_ERRORBUFFER, 1);
  }

  if ($method == 'CUSTOM-PUT') {
    curl_setopt($session, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($session, CURLOPT_POSTFIELDS, $data);
    curl_setopt($session, CURLOPT_ERRORBUFFER, 1);
  }

  if ($method == 'DELETE') {
    curl_setopt($session, CURLOPT_CUSTOMREQUEST, 'DELETE');
    curl_setopt($session, CURLOPT_POSTFIELDS, $data);
    curl_setopt($session, CURLOPT_ERRORBUFFER, 1);
  }
  
  // Make the call
  $return_data = curl_exec($session);

  // Get return http status code
  $httpcode = curl_getinfo($session, CURLINFO_HTTP_CODE);

  // Close HTTP session
  curl_close($session);

  // Prepare return
  $result = new stdClass();
  $result->code = $httpcode;
  $result->data = $return_data;

  return $result;
}
