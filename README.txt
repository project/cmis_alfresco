
Setting up a development environment should be pretty straightforward since this is mostly a vanilla installation of the products. But just so we have consistent environments here is a quick guide.
Prerequisites

    * Apache 2.x with PHP 5 (curl, json, SimpleXML)
    * MySQL
    * Java and Ant 

Setup

   1. Setup Acquia out-of-the-box install from http://acquia.com/downloads
          * Don't use straight Drupal because we'll want to use CCK shortly 
   2. Setup Alfresco Labs 3.0c out-of-the-box install
          * Assume using Tomcat bundle and configured for MySQL
          * Alfresco Labs 3 Stable (aka "3d") also works
          * Alfresco 3.x Enterprise also works. 
   3. Download code from http://www.drupal.org/project/cmis-alfresco or checkout from https://projects.optaros.com/svn/alfresco-drupal/
          * Requires the Drupal CMIS API module from http://www.drupal.org/project/cmis 
   4. Install Alfresco-Drupal modules
          * Copy (or symlink) modules/alfresco to Drupal install modules directory (sites/all/modules or sites/<site-name>/modules) 
   5. Enable the Alfresco module
   6. Set the Alfresco configuration settings
          * Service endpoint should be http://localhost:8080/alfresco 
   7. Verify by browsing to http://localhost:8080//alfresco/info 

If you are working with the checked out code, you can use Ant to deploy the module to Drupal.

   1. Create a build.properties file using the build.properties.template file provided as an example
   2. Do Ant build 