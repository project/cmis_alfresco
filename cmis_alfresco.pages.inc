<?php

/**
 * @file
 * Provides cmis_alfresco pages
 */

/**
 * Keyword based search
 * 
 * @param $keyword
 * @param $p
 */
function cmis_alfresco_opensearch_view($keyword = NULL, $page = 1) {
  module_load_include('utils.inc', 'cmis_alfresco');

  // Add opensearch form
  $contents = drupal_get_form('cmis_alfresco_opensearch_form', NULL);
  
  if ($keyword) {
        
    // Drupal paging is 0 based, adjust here
    $page   = isset($_GET['page']) ? ($_GET['page'] + 1) : $page;
    $result = cmis_alfresco_invoke_service('/api/search/keyword.atom?q='. urlencode($keyword) .'&p='. $page);
    
    if (false != $result) {
      // Process the returned XML
      $xml = cmis_alfresco_utils_get_CMIS_xml($result);
      
      // Set up results list
      $contents .= theme('cmis_alfresco_opensearch_results', $xml->xpath('//D:entry'));
      
      $opensearch = $xml->children('opensearch', TRUE);
      
      global $pager_page_array, $pager_total;
      $pager_page_array[0] = ($page - 1);
      $pager_total[0] = ceil($opensearch->totalResults / $opensearch->itemsPerPage);
      
      $contents .= theme('pager', NULL, $opensearch->itemsPerPage, 0);
    } 
    else {
      $contents .= 'Error';
    }
  }

  return $contents;  
}