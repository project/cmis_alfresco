<?php 

/**
 * Prepare and generate entry form for Alfresco settings.
 * 
 */
function cmis_alfresco_headerswing_admin_settings() {
  $form['cmis_alfresco_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Alfresco CMIS Settings'), 
    '#description' => t('Settings for Alfreso Server including Server Endpoint,'), 
    '#collapsible' => FALSE, 
    '#collapsed' => FALSE,
  );

  $form['cmis_alfresco_settings']['cmis_alfresco_headerswing_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Repository endpoint'), 
    '#description' => t('Specify the Alfresco Endpoint to integrate with.'), 
    '#default_value' => variable_get('cmis_alfresco_headerswing_endpoint', 'http://localhost:8080/alfresco'),
  	'#required' => FALSE,
  );

  $form['cmis_alfresco_settings']['cmis_alfresco_headerswing_headers'] = array(
    '#type' => 'textarea',
    '#title' => t('Headers'), 
    '#description' => t('Specify the request headers that need to be propagated to alfresco request.'), 
    '#default_value' => implode(',', variable_get('cmis_alfresco_headerswing_headers', array())),
  );
  
  return system_settings_form($form);
}

/**
 * Validate Setting inputs for Alfresco module 
 * 
 */
function cmis_alfresco_headerswing_admin_settings_validate($form, & $form_state) {
  // endpoint is required
  $endpoint = $form_state['values']['cmis_alfresco_headerswing_endpoint'];
  if ($endpoint == '') {
    form_set_error('cmis_alfresco_endpoint', t('You must specify the Alfresco endpoint.'));
  }

  // validate header list
  $header_list = $form_state['values']['cmis_alfresco_headerswing_headers'];
  $headers = array();
  foreach(explode(',', $header_list) as $header_name){
    $headers[] = trim($header_name);
  }
  $form_state['values']['cmis_alfresco_headerswing_headers'] = $headers; 
}
