<?php

/**
 * Implementation of cmisapi_getRepositoryInfo method 
 *
 */
function cmis_alfresco_cmisapi_getRepositoryInfo() {
  static $repository_info;
  
  if (empty($repository_info)) {
    module_load_include('utils.inc', 'cmis_alfresco');
    
    $response = cmis_alfresco_invoke_service('/cmis');
    $xml = cmis_alfresco_utils_get_CMIS_xml($response);
    
    if ($xml) {
      $cmis = $xml->workspace->children('cmisra', TRUE);
      $repository_info = $cmis->repositoryInfo->children('cmis', TRUE);
    }
    else {
      $repository_info = NULL;
    }
  }
  
  return $repository_info;
}

/**
 * Implementation of cmisapi_getTypes method
 * 
 * @param $repositoryId
 * @param $typeId 
 */
function cmis_alfresco_cmisapi_getTypes($repositoryId, $typeId = NULL) {
  module_load_include('utils.inc', 'cmis_alfresco');
  
  $service = $typeId?'/cmis/type/'. $typeId .'/descendants':'/cmis/types';
  $response = cmis_alfresco_invoke_service($service);

  if (FALSE != $response) {
    $items = array();
    foreach (cmis_alfresco_utils_get_CMIS_xml($response, '//D:entry') as $entry) {
      $item = _cmis_alfresco_utils_entry($entry);
      // fixing typeId, since  _cmis_alfresco_utils_entry will try to resolve type's id as a document id
      $item->id = (string) $entry->content;
      $items[] = $item;
    }
    
    return $items;
  }
  
  return FALSE;
}

/**
 * Implementation of cmisapi_getTypeDefinition
 * 
 * @param $repositoryId
 * @param $typeId
 */
function cmis_alfresco_cmisapi_getTypeDefinition($repositoryId, $typeId) {
  module_load_include('utils.inc', 'cmis_alfresco');

  $response = cmis_alfresco_invoke_service('/cmis/type/'. $typeId);
  $types = _cmis_alfresco_utils_known_property_types();
  
  if (FALSE != $response) {
    $xml = cmis_alfresco_utils_get_CMIS_xml($response);
    $cmis_type = _cmis_alfresco_utils_entry($xml);
    $cmis_element = $xml->children('cmisra', TRUE);
    $properties_element = $cmis_element->children('cmis', TRUE);
        
    if ($properties_element) {
      foreach ($types as $type) {
        $tag = 'property'. $type .'Definition';
        foreach ($properties_element->$tag as $property) {
          $attrs = $property->attributes('cmis', TRUE);
          $cmis_type->fields[ (string) $property->localName ] = array(
            'id' => (string) $property->id, 
            'name' => (string) $property->localName,
            'displayName' => (string) $property->displayName,
            'propertyType' => (string) $property->propertyType
          );
        }
      }
    }
    
    return $cmis_type;
  }
  
  return FALSE;
}

/**
 * Implementation of cmisapi_getChildren method
 * 
 * @param $repositoryId
 * @param $folderId
 * @param $type
 * @param $filter
 * @param $includeAllowableActions
 * @param $includeRelationships
 * @param $maxItems
 * @param $skipCount
 * @param $orderBy
 */
function cmis_alfresco_cmisapi_getChildren($repositoryId, $folderId, $type = NULL, $filter = NULL, $includeAllowableActions = NULL, $includeRelationships = NULL, $maxItems = NULL, $skipCount = NULL, $orderBy = NULL) {
  module_load_include('utils.inc', 'cmis_alfresco');

  $folderId = cmis_alfresco_objectId($folderId);
  if ($folderId['noderef_url']) {
    $url = '/cmis/'. $folderId['noderef_url'];
  }
  else {
    $url = $folderId['url'];
  }

  $args_clean = array();
  $args = array('type' => $type, 'filter' => $filter, 'includeAllowableActions' => $includeAllowableActions, 
  	'includeRelationships' => $includeRelationships, 'maxItems' => $maxItems, 'skipCount' => $skipCount, 'orderBy' => $orderBy);
  foreach ($args as $key => $value) {
    if (!empty($value)) {
      $args_clean[$key] = $value;
    }
  }

  $response = cmis_alfresco_invoke_service($url .'/children'. (!empty($args_clean)?'?'. drupal_query_string_encode($args_clean):''));
  
  if (FALSE != $response) {
    return _cmis_alfresco_getEntries(cmis_alfresco_utils_get_CMIS_xml($response, '//D:entry'));
  }

  return FALSE;
}

/**
 * Implementation of cmisapi_getObjectParents method
 * 
 * @param $repositoryId
 * @param $folderId
 */
function cmis_alfresco_cmisapi_getObjectParents($repositoryId, $folderId, $filter = NULL, $includeAllowableActions = NULL, $includeRelationships = NULL) {
  module_load_include('utils.inc', 'cmis_alfresco');

  $folderId = cmis_alfresco_objectId($folderId);
  if ($folderId['noderef_url']) {
    $url = '/cmis/'. $folderId['noderef_url'];
  }
  else {
    $url = $folderId['url'];
  }
  
  $args_clean = array();
  $args = array('filter' => $filter, 'includeAllowableActions' => $includeAllowableActions, 'includeRelationships' => $includeRelationships);
  foreach ($args as $key => $value) {
    if (!empty($value)) {
      $args_clean[$key] = $value;
    }
  }

  $response = cmis_alfresco_invoke_service($url .'/parent'. (!empty($args_clean)?'?'. drupal_query_string_encode($args_clean):''));
  
  if (FALSE != $response) {
    return _cmis_alfresco_getEntries(cmis_alfresco_utils_get_CMIS_xml($response, '//D:entry'));
  }

  return FALSE;
}

/**
 * Implementation of cmisapi_createDocument method 
 * 
 * @param $repositoryId
 * @param $objectTypeId
 * @param $properties
 * @param $folderId
 * @param $content
 * @param $versioningState
 */
function cmis_alfresco_cmisapi_createDocument($repositoryId, $objectTypeId = 'cmis:document', $properties = array(), $folderId = NULL, $content = NULL, $versioningState = NULL) {
  module_load_include('utils.inc', 'cmis_alfresco');

  $parentFolderId = cmis_alfresco_objectId($folderId);
  if (!is_array($parentFolderId) || !array_key_exists('noderef_url', $parentFolderId)) {
    throw new CMISException('Unable to find destination folder: '. $folderId);
  }

  $title = $properties['title'];
  unset($properties['title']);
  
  $summary = $properties['summary'];
  unset($properties['summary']);
  
  $content_type = $properties['content-type'];
  unset($properties['content-type']);
  
  $properties['objectTypeId'] = $objectTypeId;
  $properties_xml = _cmis_alfresco_utils_properties_to_xml($properties);  
    
  $postvars = '<?xml version="1.0" encoding="utf-8"?>'.
  '<entry xmlns="http://www.w3.org/2005/Atom" '.
    ' xmlns:cmisra="http://docs.oasis-open.org/ns/cmis/restatom/200908/"'.
    ' xmlns:cmis="http://docs.oasis-open.org/ns/cmis/core/200908/">'.
    '<title>'. $title .'</title>'.
    '<summary>'. $summary .'</summary>'.
    '<content type="'. $content_type .'">'. base64_encode($content) .'</content>'.
    '<cmisra:object>'.
      '<cmis:properties>'. $properties_xml .'</cmis:properties>'.
    '</cmisra:object>'.
  '</entry>';

  $header[] = 'Content-type: application/atom+xml;type=entry';
  $header[] = 'Content-length: '. strlen($postvars);
  $header[] = 'MIME-Version: 1.0';

  $response = cmis_alfresco_invoke_service('/cmis/'. $parentFolderId['noderef_url'] .'/children', $header, 'CUSTOM-POST', $postvars);
  $entry = cmis_alfresco_utils_get_CMIS_xml($response, '/D:entry');
  $entry = $entry[0][0];

  $objectId = cmis_alfresco_objectId((string)$entry->id);

  return $objectId['noderef'];
}

/**
 * Implementation of cmisapi_createFolder method 
 * 
 * @param $repositoryId
 * @param $objectTypeId
 * @param $properties
 * @param $folderId
 */
function cmis_alfresco_cmisapi_createFolder($repositoryId, $objectTypeId = 'cmis:folder', $properties = array(), $folderId = NULL) {
  module_load_include('utils.inc', 'cmis_alfresco');

  $parentFolderId = cmis_alfresco_objectId($folderId);
  if (!is_array($parentFolderId) || !array_key_exists('noderef_url', $parentFolderId)) {
    throw new CMISException('Unable to find destination folder: '. $folderId);
  }

  $title = $properties['title'];
  unset($properties['title']);
   
  $properties['ObjectTypeId'] = $objectTypeId;
  $properties_xml = _cmis_alfresco_utils_properties_to_xml($properties);  
  
  $postvars = '<?xml version="1.0" encoding="utf-8"?>'.
  '<entry xmlns="http://www.w3.org/2005/Atom"'.
    ' xmlns:cmisra="http://docs.oasis-open.org/ns/cmis/restatom/200908/"'.
    ' xmlns:cmis="http://docs.oasis-open.org/ns/cmis/core/200908/">'.
  '<title>'. $title .'</title>'.
  '<cmisra:object>'.
    '<cmis:properties>'.
      '<cmis:propertyId propertyDefinitionId="cmis:objectTypeId">'.
      '<cmis:value>cmis:folder</cmis:value>'.
      '</cmis:propertyId>'.
    '</cmis:properties>'.
  '</cmisra:object>'.
  '</entry>';

  $header[] = 'Content-type: application/atom+xml';
  $header[] = 'Content-length: '. strlen($postvars);
  $header[] = 'MIME-Version: 1.0';
  
  $response = cmis_alfresco_invoke_service('/cmis/'. $parentFolderId['noderef_url'] .'/children', $header, 'CUSTOM-POST', $postvars);
  $entry = cmis_alfresco_utils_get_CMIS_xml($response, '/D:entry');
  $entry = $entry[0][0];

  $objectId = cmis_alfresco_objectId((string)$entry->id);

  return $objectId['noderef'];
}

/**
 * Implementation of cmisapi_getProperties method
 * 
 * @param $repositoryId
 * @param $objectId
 */
function cmis_alfresco_cmisapi_getProperties($repositoryId, $objectId) {
  module_load_include('utils.inc', 'cmis_alfresco');
  $objectId = cmis_alfresco_objectId($objectId);

  if ($response = cmis_alfresco_invoke_service('/cmis/'. $objectId['noderef_url'])) {
    if ($entries = _cmis_alfresco_getEntries(cmis_alfresco_utils_get_CMIS_xml($response, '//D:entry'))) {
      return $entries[0];
    }
  }

  watchdog('cmis_alfresco_cmisapi_getProperties', 'Unknown objectId "@objectId"', array('@objectId' => $objectId));
  return FALSE;
}

/**
 * Implementation of cmisapi_updateProperties method
 * 
 * @param $repositoryId
 * @param $objectId
 * @param $changeToken
 * @param $propertyCollection
 */
function cmis_alfresco_cmisapi_updateProperties($repositoryId, $objectId, $changeToken = NULL, $propertyCollection = array()) {
  module_load_include('utils.inc', 'cmis_alfresco');

  $alfresco_objectId = cmis_alfresco_objectId($objectId);
  if (!is_array($alfresco_objectId) || !array_key_exists('noderef_url', $alfresco_objectId)) {
    drupal_set_message('Unable to find destination object: '. $objectId, 'error');
    return FALSE;
  }

  $title = $propertyCollection['title'];
  unset($propertyCollection['title']);
  
  $summary = $propertyCollection['summary'];
  unset($propertyCollection['summary']);
  
  $content_type = $propertyCollection['content-type'];
  unset($propertyCollection['content-type']);

  $properties_xml = _cmis_alfresco_utils_properties_to_xml($propertyCollection);
  
  $postvars = '<?xml version="1.0" encoding="utf-8"?>'.
  '<entry xmlns="http://www.w3.org/2005/Atom" '.
    ' xmlns:cmisra="http://docs.oasis-open.org/ns/cmis/restatom/200908/"'.
    ' xmlns:cmis="http://docs.oasis-open.org/ns/cmis/core/200908/">'.
    '<cmisra:object>'.
      '<cmis:properties>'. $properties_xml .'</cmis:properties>'.
    '</cmisra:object>'.
  '</entry>';

  $header[] = 'Content-type: application/atom+xml;type=entry';
  $header[] = 'Content-length: '. strlen($postvars);
  $header[] = 'MIME-Version: 1.0';

  $response = cmis_alfresco_invoke_service('/cmis/'. $alfresco_objectId['noderef_url'], $header, 'CUSTOM-PUT', $postvars);
  $entry = cmis_alfresco_utils_get_CMIS_xml($response, '/D:entry');
  $entry = $entry[0][0];

  $result = cmis_alfresco_objectId((string) $entry->id);

  return $result['noderef'];
}

/**
 * Implemetation of cmisapi_getContentStream method
 * 
 * @param $repositoryId
 * @param $objectId
 */
function cmis_alfresco_cmisapi_getContentStream($repositoryId, $objectId) {
  module_load_include('utils.inc', 'cmis_alfresco');
  
  $objectId = cmis_alfresco_objectId($objectId);
  $cmis_object = cmis_alfresco_cmisapi_getProperties($repositoryId, $objectId);
  return cmis_alfresco_invoke_service('/cmis/'. $objectId['noderef_url'] .'/content');
}

/**
 * Implementation of cmisapi_setContentStream method
 * 
 * @param $repositoryId
 * @param $objectId
 * @param $overwriteFlag
 * @param $content
 * @param $properties
 * 
 * @todo 
 *   send all known properties, see cmis_alfresco_cmisapi_updateProperties
 */
function cmis_alfresco_cmisapi_setContentStream($repositoryId, $objectId, $overwriteFlag = TRUE, $content = NULL, $properties = array()) {
  module_load_include('utils.inc', 'cmis_alfresco');

  $title = $properties['title'];
  unset($properties['title']);
  
  $summary = $properties['summary'];
  unset($properties['summary']);
  
  $content_type = $properties['content-type'];
  unset($properties['content-type']);

  $objectId = cmis_alfresco_objectId($objectId);
  $properties_xml = _cmis_alfresco_utils_properties_to_xml($properties);  
  
  $postvars = '<?xml version="1.0" encoding="utf-8"?>'.
  '<entry xmlns="http://www.w3.org/2005/Atom" '.
    ' xmlns:cmisra="http://docs.oasis-open.org/ns/cmis/restatom/200908/"'.
    ' xmlns:cmis="http://docs.oasis-open.org/ns/cmis/core/200908/">'.
    '<title>'. $title .'</title>'.
    '<content type="'. $content_type .'">'. base64_encode($content) .'</content>'.
    '<cmisra:object>'.
      '<cmis:properties>'. $properties_xml .'</cmis:properties>'.
    '</cmisra:object>'.
  '</entry>';

  $header[] = 'Content-type: application/atom+xml;type=entry';
  $header[] = 'Content-length: '. strlen($postvars);
  $header[] = 'MIME-Version: 1.0';

  $objectId = cmis_alfresco_objectId($objectId);
  $response = cmis_alfresco_invoke_service('/cmis/'. $objectId['noderef_url'], $header, 'CUSTOM-PUT', $postvars);
  return $response;
}

/**
 * Implementation of cmisapi_query method
 *   
 * @param $repositoryId
 * @param $statement
 * @param $searchAllVersions
 * @param $includeAllAllowableActions
 * @param $includeRelationships
 * @param $maxItems
 * @param $skipCount
 * @return array
 */
function cmis_alfresco_cmisapi_query($repositoryId, $statement, $searchAllVersions = FALSE, $includeAllAllowableActions = FALSE, $includeRelationships = FALSE, $maxItems = NULL, $skipCount = NULL) {
  module_load_include('utils.inc', 'cmis_alfresco');

  $postvars='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'.
  '<cmis:query xmlns:cmis="http://docs.oasis-open.org/ns/cmis/core/200908/">'.
    '<cmis:statement><![CDATA['. $statement .']]></cmis:statement>'.
    (empty($skipCount)?'':'<cmis:skipCount>'. $skipCount .'</cmis:skipCount>').
    (empty($maxItems)?'':'<cmis:maxItems>'. $maxItems .'</cmis:maxItems>').
  '</cmis:query>';

  $header[] = 'Content-type: application/cmisquery+xml';

  $response = cmis_alfresco_invoke_service('/cmis/queries', $header, 'CUSTOM-POST', $postvars);

  if (FALSE != $response) {
    return _cmis_alfresco_getEntries(cmis_alfresco_utils_get_CMIS_xml($response, '//D:entry'));
  }

  return FALSE;
}

/**
 * Implementation of cmisapi_deleteObject method
 * 
 * @param $repositoryId
 * @param $objectId
 */
function cmis_alfresco_cmisapi_deleteObject($repositoryId, $objectId) {
  module_load_include('utils.inc', 'cmis_alfresco');

  $objectId = cmis_alfresco_objectId($objectId);  
  $header[] = 'MIME-Version: 1.0';

  $response = cmis_alfresco_invoke_service('/cmis/'. $objectId['noderef_url'], $header, 'DELETE', '');
}
